# Wallbox

Wallbox is a REST API service for providing daily reports about a charger's charging activity to charger owners.
It handles the following functionalities:

1.  registering and updating chargers
2.  linking chargers to their owners
3.  saving charging sessions data sent by chargers
4.  turning on and off the daily report send out for a specific charger
5.  triggering the generation of the daily report of chargers for the respective owner

## Installation

1. Install node packages by running the following command:

```bash
npm install
```
2. To be able to run the project, make sure you have a database `wallbox` created. This project uses Microsoft SQL Server, but it could be easily switched to a different one because of the usage of TypeORM.

3. Edit the `ormconfig.json` and `app.ts` files and put your own database connection configuration options in there.
   Particularly, you'll probably need to configure host, username, password, database, and maybe port options.

4. Once you finish with configuration and all node modules are installed, you can run the application:
```bash
npm start
```
## Usage

For testing purposes, you can refer to the [postman collection](https://www.getpostman.com/collections/d71064922cead6659165).

## Further work
This project is obviously not following TDD principles and is missing tests, which should be added before going to production.