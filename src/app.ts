import * as cron from 'node-cron';
import * as express from 'express';
import "reflect-metadata";
import { createConnection } from "typeorm";
import {DailyReportsService} from "./services/DailyReportsService";
const http = require('http');

class App {
    public app: express.Application;
    public port: number;

    constructor(appInit: { port: number; controllers?: any; helpers?: any, scheduler?: any}) {
        this.app = express();
        this.port = appInit.port;
        this.helpers(appInit.helpers);
        this.routes(appInit.controllers);
        createConnection({
            type: "mssql",
            host: "localhost",
            port: 1433,
            username: "admin",
            password: "admin",
            database: "wallbox",
            options: {
                encrypt: false
            },
            entities: [
                "src/entity/**/*.ts"
            ],
            synchronize: true,
            logging: false
        })
            .then(() => {
                this.scheduleDailyReports(appInit.scheduler);
            })
            .catch(error =>
                console.log(error)
            );
    }

    private scheduleDailyReports(scheduler: DailyReportsService) {
        //schedule crone job for generating daily reports every day at 23:59:59
        // cron.schedule('59 59 23 * * *', async () => {
        //     await scheduler.generateReports();
        // });
        cron.schedule('1 * * * * *', async () => {
            await scheduler.generateReports();
        });
    }

    private helpers(helpers: { forEach: (arg0: (helper: any) => void) => void; }) {
        helpers.forEach(helper => {
            this.app.use(helper)
        })
    }

    private routes(controllers: { forEach: (arg0: (controller: any) => void) => void; }) {
        controllers.forEach(controller => {
            this.app.use('/', controller.router)
        })
    }

    public listen() {
        const server = http.createServer(this.app);
        server.listen(this.port, () => {
            console.log(`App listening on port: ${this.port}`)
        })
    }
}

export default App