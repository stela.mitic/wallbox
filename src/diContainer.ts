import {BaseController} from "./controllers/BaseController";
import {ChargerController} from "./controllers/ChargerController";
import {ChargerService} from "./services/ChargerService";
import {ChargingSessionController} from "./controllers/ChargingSessionController";
import {ChargingSessionService} from "./services/ChargingSessionService";
import {DailyReportsService} from "./services/DailyReportsService";

export class DiContainer {
    private chargerService: ChargerService;
    private chargingSessionService: ChargingSessionService;

    constructor() {
        this.createServices();
    }

    private createServices() {
        this.chargerService = new ChargerService();
        this.chargingSessionService = new ChargingSessionService();
    }

    public createControllers(): BaseController[] {

        let controllers : BaseController[] = [];

        const chargerController = new ChargerController('charger', this.chargerService);
        controllers.push(chargerController);

        const chargingSessionController = new ChargingSessionController('charging-session', this.chargingSessionService);
        controllers.push(chargingSessionController);

        return controllers;
    }

    public createSchedulers(): DailyReportsService {
        return new DailyReportsService();
    }
}