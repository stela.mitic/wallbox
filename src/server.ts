import {config} from 'dotenv';
import App from "./app";
import * as bodyParser from 'body-parser'
import {DiContainer} from "./diContainer";

config();
const diContainer : DiContainer = new DiContainer();

const app = new App({
    port : 3001,
    controllers: diContainer.createControllers(),
    helpers: [
        bodyParser.json(),
        bodyParser.urlencoded({ extended: true })
    ],
    scheduler: diContainer.createSchedulers()
});

app.listen();