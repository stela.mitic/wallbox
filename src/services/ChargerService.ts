import {getRepository} from "typeorm";
import {Charger} from "../entity/Charger";
import {RegisterChargerRequest} from "../models/requests/charger/RegisterChargerRequest";
import {LinkChargerToNewOwnerRequest} from "../models/requests/charger/LinkChargerToNewOwnerRequest";
import {AllowDailyReportRequest} from "../models/requests/charger/AllowDailyReportRequest";
import {UpdateChargerRequest} from "../models/requests/charger/UpdateChargerRequest";
import {Owner} from "../entity/Owner";
import {LinkChargerToExistingOwnerRequest} from "../models/requests/charger/LinkChargerToExistingOwnerRequest";

export class ChargerService {
    async registerCharger(registerChargerRequest: RegisterChargerRequest): Promise<Charger> {
        let charger = new Charger();
        charger.serialNumber = registerChargerRequest.serialNumber;
        return await getRepository(Charger).save(charger);
    }

    async updateCharger(chargerId: string, updateChargerRequest: UpdateChargerRequest): Promise<Charger> {
        let chargerRepository = getRepository(Charger);
        let charger = await chargerRepository.findOne(chargerId);
        if (typeof charger !== 'undefined') {
            charger.serialNumber = updateChargerRequest.serialNumber;
            return await chargerRepository.save(charger);
        }
        else {
            throw new Error ('Charger could not be found.');
        }
    }

    async linkToNewOwner(linkChargerToOwnerRequest: LinkChargerToNewOwnerRequest): Promise<Charger> {
        let ownerRepository = getRepository(Owner);
        let charger = await this.getCharger(linkChargerToOwnerRequest.chargerId);
        if (typeof charger !== 'undefined') {
            let owner = new Owner();
            owner.fullName = linkChargerToOwnerRequest.ownerFullName;
            owner.email = linkChargerToOwnerRequest.ownerEmail;
            charger.owner = owner;
            await ownerRepository.save(owner);
            return await getRepository(Charger).save(charger);
        }
        else {
            throw new Error ('Charger could not be found.');
        }
    }

    async linkToExistingOwner(linkChargerToOwnerRequest: LinkChargerToExistingOwnerRequest): Promise<Charger> {
        let ownerRepository = getRepository(Owner);
        let owner = await ownerRepository.findOne(linkChargerToOwnerRequest.ownerId);
        if (typeof owner !== 'undefined') {
            let charger = await this.getCharger(linkChargerToOwnerRequest.chargerId);
            if (typeof charger !== 'undefined') {
                charger.owner = owner;
                return await getRepository(Charger).save(charger);
            }
            else {
                throw new Error ('Charger could not be found.');
            }
        }
        else {
            throw new Error ('Owner could not be found.');
        }
    }

    async getCharger(chargerId: string): Promise<Charger> {
        let chargerRepository = getRepository(Charger);
        let charger = await chargerRepository.findOne(chargerId);
        if (typeof charger !== 'undefined') {
            return charger;
        }
        else {
            throw new Error ('Charger could not be found.');
        }
    }

    async allowDailyReport(chargerId: string, allowDailyReportRequest: AllowDailyReportRequest): Promise<Charger> {
        let chargerRepository = getRepository(Charger);
        let charger = await chargerRepository.findOne(chargerId);
        if (typeof charger !== 'undefined') {
            charger.allowDailyReport = allowDailyReportRequest.allowDailyReport;
            return await chargerRepository.save(charger);
        }
        else {
            throw new Error ('Charger could not be found.');
        }
    }
}