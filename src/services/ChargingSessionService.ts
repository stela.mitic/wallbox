import {getRepository} from "typeorm";
import {SaveChargingSessionRequest} from "../models/requests/charging_session/SaveChargingSessionRequest";
import {Charger} from "../entity/Charger";
import {ChargingSession} from "../entity/ChargingSession";

export class ChargingSessionService {
    async saveChargingSession(chargerId: string, saveChargingSessionRequest: SaveChargingSessionRequest): Promise<ChargingSession> {
        let chargerRepository = getRepository(Charger);
        let charger = await chargerRepository.findOne(chargerId);
        if (typeof charger !== 'undefined') {
            let chargingSession = new ChargingSession();
            chargingSession.charger = charger;
            chargingSession.batteryStatus = saveChargingSessionRequest.batteryStatus;
            chargingSession.duration = saveChargingSessionRequest.duration;
            chargingSession.date = new Date();
            return await getRepository(ChargingSession).save(chargingSession);
        }
        else {
            throw new Error ('Charger could not be found.');
        }
    }
}