import {getConnection, getRepository} from "typeorm";
import {Report} from "../models/Report";
import {Owner} from "../entity/Owner";
import {Charger} from "../entity/Charger";
const moment = require('moment');

export class DailyReportsService {
    async generateReports(): Promise<any> {
        let allOwners = await this.getAllOwners();
        for (let owner of allOwners) {
            let reports = await this.generateReportsForOwner(owner.id);
            //this is the part where an email-service which handles both html template generation and sending email would be called
            //this.emailService.sendEmail(owner, reports);
            if (reports.length > 0) {
                console.log("Daily reports for the owner " + owner.fullName + "\n");
                console.log(reports);
            }
        }
    }

    async getAllOwners(): Promise<Owner[]> {
        let ownerRepository = getRepository(Owner);
        return await ownerRepository.find();
    }

    async generateReportsForOwner(ownerId: number): Promise<Report[]> {
        let start = new Date();
        let formattedDate = moment(start).format('D MMMM YYYY');

        start.setUTCHours(0,0,0,0);
        let date = new Date(start.getTime()).toISOString();
        let reports = await getConnection()
            .createQueryBuilder()
            .select(['charger.id', 'charger.serialNumber', 'owner.id'])
            .addSelect('count(charging_session.id) as charging_sessions_count')
            .addSelect("SUM(charging_session.duration)", "total_duration")
            .from(Charger, 'charger')
            .innerJoin('charger.owner', 'owner')
            .innerJoin('charger.chargingSessions', 'charging_session')
            .where('charger.allowDailyReport = :flag', { flag: true })
            .andWhere('charging_session.date >= :date', {date: date})
            .andWhere('owner.id = :ownerId', {ownerId: ownerId})
            .groupBy('owner.id')
            .addGroupBy('charger.id')
            .addGroupBy('charger.serialNumber')
            .getRawMany();

        let formattedReports = [];
        reports.forEach(report => {
            let formattedReport: Report = {
                serialNumber: report.charger_serialNumber,
                date: formattedDate,
                chargingSessionsCount: report.charging_sessions_count,
                totalDuration: report.total_duration
            }
            formattedReports.push(formattedReport);
        })

        return formattedReports;
    }
}