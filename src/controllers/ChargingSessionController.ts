import {BaseController} from "./BaseController";
import {Request, Response} from "express";
import {ChargingSessionService} from "../services/ChargingSessionService";
import {SaveChargingSessionRequest} from "../models/requests/charging_session/SaveChargingSessionRequest";
import {ChargingSession} from "../entity/ChargingSession";

export class ChargingSessionController extends BaseController {
    private chargingSessionService: ChargingSessionService;

    constructor(path: string, chargingSessionService: ChargingSessionService) {
        super(path);
        this.chargingSessionService = chargingSessionService;
        this.initRoutes();
    }

    private initRoutes(): void {
        this.router.post(`${this.path}/save-charging-session`, this.saveChargingSession);
    }

    private saveChargingSession = (req: Request, res: Response) => {
        let chargerId = req.query.id as string;
        const saveChargingSessionRequest: SaveChargingSessionRequest = req.body;
        return this.chargingSessionService.saveChargingSession(chargerId, saveChargingSessionRequest)
            .then((chargingSession: ChargingSession) => {
                this.sendValidResponse(chargingSession, res);
            })
            .catch(error => {
                this.sendErrorResponse(error, res);
            })
    };
}