import {BaseController} from "./BaseController";
import {Request, Response} from "express";
import {ChargerService} from "../services/ChargerService";
import {Charger} from "../entity/Charger";
import {RegisterChargerRequest} from "../models/requests/charger/RegisterChargerRequest";
import {LinkChargerToNewOwnerRequest} from "../models/requests/charger/LinkChargerToNewOwnerRequest";
import {AllowDailyReportRequest} from "../models/requests/charger/AllowDailyReportRequest";
import {UpdateChargerRequest} from "../models/requests/charger/UpdateChargerRequest";
import {LinkChargerToExistingOwnerRequest} from "../models/requests/charger/LinkChargerToExistingOwnerRequest";

export class ChargerController extends BaseController {
    private chargerService: ChargerService;

    constructor(path: string, chargerService: ChargerService) {
        super(path);
        this.chargerService = chargerService;
        this.initRoutes();
    }

    private initRoutes(): void {
        this.router.post(`${this.path}/register-charger`, this.registerCharger);
        this.router.put(`${this.path}/update-charger`, this.updateCharger);
        this.router.post(`${this.path}/link-charger-to-new-owner`, this.linkChargerToNewOwner);
        this.router.put(`${this.path}/link-charger-to-existing-owner`, this.linkChargerToExistingOwner);
        this.router.put(`${this.path}/allow-daily-report`, this.allowDailyReport);
    }

    private registerCharger = (req: Request, res: Response) => {
        const registerChargerRequest: RegisterChargerRequest = req.body;
        return this.chargerService.registerCharger(registerChargerRequest)
            .then((charger: Charger) => {
                this.sendValidResponse(charger, res);
            })
            .catch(error => {
                this.sendErrorResponse(error, res);
            })
    };

    private updateCharger = (req: Request, res: Response) => {
        let chargerId = req.query.id as string;
        const updateChargerRequest: UpdateChargerRequest = req.body;
        return this.chargerService.updateCharger(chargerId, updateChargerRequest)
            .then((charger: Charger) => {
                this.sendValidResponse(charger, res);
            })
            .catch(error => {
                this.sendErrorResponse(error, res);
            })
    };

    private linkChargerToNewOwner = (req: Request, res: Response) => {
        const linkChargerToOwnerRequest: LinkChargerToNewOwnerRequest = req.body;
        return this.chargerService.linkToNewOwner(linkChargerToOwnerRequest)
            .then((charger: Charger) => {
                this.sendValidResponse(charger, res);
            })
            .catch(error => {
                this.sendErrorResponse(error, res);
            })
    };

    private linkChargerToExistingOwner = (req: Request, res: Response) => {
        const linkChargerToOwnerRequest: LinkChargerToExistingOwnerRequest = req.body;
        return this.chargerService.linkToExistingOwner(linkChargerToOwnerRequest)
            .then((charger: Charger) => {
                this.sendValidResponse(charger, res);
            })
            .catch(error => {
                this.sendErrorResponse(error, res);
            })
    };

    private allowDailyReport = (req: Request, res: Response) => {
        let chargerId = req.query.id as string;
        const allowDailyReportRequest: AllowDailyReportRequest = req.body;
        return this.chargerService.allowDailyReport(chargerId, allowDailyReportRequest)
            .then((charger: Charger) => {
                this.sendValidResponse(charger, res);
            })
            .catch(error => {
                this.sendErrorResponse(error, res);
            })
    };
}