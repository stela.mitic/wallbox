import * as express from 'express';
import {Response} from "express";

export class BaseController {
    protected path;
    protected router;

    protected constructor(path : string) {
        this.path = '/' + path;
        this.router = express.Router();
    }

    sendValidResponse = ((content: any, res: Response, status?: number) => {
        const responseObject = {
            data: content,
            message: '',
            status: (typeof status !== 'undefined') ? status : 200
        };
        res.status(200).header("Content-Type",'application/json').json(responseObject);
    });

    sendErrorResponse = ((err: Error, res: Response, status?: number) => {
        const responseObject = {
            data: null,
            message: err.message,
            status: (typeof status != 'undefined') ? status : 400
        };
        res.header("Content-Type",'application/json').json(responseObject);
    });
}