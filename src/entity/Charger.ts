import {Entity, Column, PrimaryGeneratedColumn, ManyToOne, OneToMany, JoinColumn} from "typeorm";
import {ChargingSession} from "./ChargingSession";
import {Owner} from "./Owner";

@Entity()
export class Charger {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: "varchar",
        unique: true,
        nullable: false,
    })
    serialNumber: string;

    @ManyToOne(() => Owner, owner => owner.chargers)
    @JoinColumn({ name: "ownerId" })
    owner: Owner;

    @Column({
        type: "bit",
        unique: false,
        nullable: true,
    })
    allowDailyReport: boolean;

    @OneToMany(() => ChargingSession, chargingSession => chargingSession.charger)
    chargingSessions: ChargingSession[];
}