import {Entity, PrimaryGeneratedColumn, Column, OneToMany} from "typeorm";
import {Charger} from "./Charger";

@Entity()
export class Owner {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: "varchar",
        unique: false,
        nullable: false,
    })
    fullName: string;

    @Column({
        type: "varchar",
        unique: true,
        nullable: false,
    })
    email: string;

    @OneToMany(() => Charger, charger => charger.owner)
    chargers: Charger[];
}