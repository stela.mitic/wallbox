import {Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn} from "typeorm";
import {Charger} from "./Charger";

@Entity()
export class ChargingSession {
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(() => Charger, charger => charger.chargingSessions)
    @JoinColumn({ name: "chargerId" })
    charger: Charger;

    @Column()
    date: Date;

    @Column()
    duration: number;

    @Column()
    batteryStatus: number;
}