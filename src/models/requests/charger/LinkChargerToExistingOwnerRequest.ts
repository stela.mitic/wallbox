export interface LinkChargerToExistingOwnerRequest {
    chargerId: string;
    ownerId: string;
}