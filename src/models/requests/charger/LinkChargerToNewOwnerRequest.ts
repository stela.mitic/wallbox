export interface LinkChargerToNewOwnerRequest {
    chargerId: string;
    ownerFullName: string;
    ownerEmail: string;
}