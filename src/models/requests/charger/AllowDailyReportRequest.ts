export interface AllowDailyReportRequest {
    allowDailyReport: boolean;
}