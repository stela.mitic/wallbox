export interface SaveChargingSessionRequest {
    chargerId: string;
    batteryStatus: number;
    duration: number;
}