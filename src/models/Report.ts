export interface Report {
    serialNumber: string;
    date: string;
    chargingSessionsCount: number;
    totalDuration: number;
}